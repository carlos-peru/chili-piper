import React from "react";
import ExpandableBox from "./ExpandableBox";
import Algorithm from "./Algorithm";
import MeetingSettings from "./MeetingSettings";
import OnlineBookingUrls from "./OnlineBookingUrls";
import Rules from "./Rules";

export default class BookingLinks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editableItems: [
        ["Rules", <Rules />],
        ["Algorithm", <Algorithm />],
        ["Online Booking URL", <OnlineBookingUrls />],
        ["Meeting Settings", <MeetingSettings />]
      ]
    };
  }
  render() {
    return (
      <div className="booking">
        {this.state.editableItems.map((editableItem, index) => {
          return (
            <ExpandableBox key={index} title={editableItem[0]}>
              {editableItem[1]}
            </ExpandableBox>
          );
        })}
        <style jsx>{`
          div.booking {
            width: 80%;
            margin: 20px;
            padding: 0;
            height: 100%;
            position: relative;
            background: #fff;
          }
        `}</style>
      </div>
    );
  }
}
