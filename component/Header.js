import React from "react";
import Link from "next/link";
import HeaderMenu from "./HeaderMenu";
import ChiliPiper from "../assets/svgs/chili-piper.svg";

export default class Header extends React.Component {
  render() {
    const { pathname } = this.props;
    return (
      <header>
        <div className="logo">
          <h1>
            <Link prefetch href="/">
              <a>
                <ChiliPiper />
              </a>
            </Link>
          </h1>
        </div>
        <div style={{ width: "50%" }}>
          <HeaderMenu pathname={pathname} />
        </div>

        <div className="corner">
          <div className="avatar" />
        </div>

        <style jsx>{`
          header {
            justify-content: space-between;
            display: flex;
            overflow: hidden;
            background-color: #ffffff;
            border-bottom: 1px solid #808b97;
          }

          header a {
            text-decoration: none;
          }

          .logo {
            padding: 0 16px;
            margin-left: 1rem;
            width: 25%;
          }
          .logo h1 {
            vertical-align: middle;
          }

          .logo a {
            color: white;
          }
          .corner {
            width: 25%;
            display: block;
          }
          .avatar {
            display: relative;
            margin: 25px;
            float: right;
            height: 32px;
            width: 32px;
            background: red;
            border-radius: 50%;
          }
          @media only screen and (max-width: 680px) {
            // TODO: support mobile
          }
        `}</style>
      </header>
    );
  }
}
