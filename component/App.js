import Head from "next/head";
import Header from "./Header";
import Main from "./Main";

export default ({ children, pathname }) => (
  <div>
    <Head>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
      />
      <title>Chili Piper</title>
    </Head>
    <div>
      <Header pathname={pathname} />
      <Main>{children}</Main>
      <style jsx global>{`
        * {
          font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
        body {
          position: relative;
          margin: 0;
          padding: 0;
          height: 100%;
          background: #f7f7f8;
        }
      `}</style>
    </div>
  </div>
);
