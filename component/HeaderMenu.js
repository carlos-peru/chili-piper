import React from "react";
import Link from "next/link";
import Email from "../assets/svgs/email.svg";
import Notes from "../assets/svgs/notes.svg";
import Settings from "../assets/svgs/settings.svg";
import Tasks from "../assets/svgs/tasks.svg";

export default class HeaderMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuItems: [
        ["/calendar", "Calendar", <Email />],
        ["/notes", "Notes", <Notes />],
        ["/tasks", "Tasks", <Tasks />],
        ["/email", "Email", <Email />],
        ["/settings", "Settings", <Settings />]
      ]
    };
  }

  render() {
    const { pathname } = this.props;
    return (
      <nav>
        {this.state.menuItems.map((menuItem, index) => {
          return (
            <Link key={index} prefetch href={menuItem[0]}>
              <a className={pathname === menuItem[0] && "active"}>
                <i>{menuItem[2]}</i>
                <span>{menuItem[1]}</span>
              </a>
            </Link>
          );
        })}
        <style jsx>{`
          nav {
            padding: 14px 16px;
            margin: 1rem;
            backgroung: blue;
          }

          nav a {
            text-decoration: none;
            padding: 14px 16px;
            color: #808b97;
            text-align: center;
            font-size: 16px;
          }
          nav a i {
            padding: 5px;
            vertical-align: middle;
          }

          nav a:hover {
          }

          nav a.active {
            border-radius: 5px;
            text-decoration: underline;
          }
          @media only screen and (max-width: 680px) {
            // TODO: support mobile
          }
        `}</style>
      </nav>
    );
  }
}
