import React from "react";
import Link from "next/link";
import Events from "../assets/svgs/events.svg";
import BookingLink from "../assets/svgs/link.svg";
import Reports from "../assets/svgs/reports.svg";

export default class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuItems: [
        ["/events", "Events", <Events />],
        ["/reports", "Reports", <Reports />],
        ["/", "Booking Links", <BookingLink />]
      ]
    };
  }

  render() {
    const { pathname } = this.props;
    return (
      <main>
        <div className="sidebar">
          <h3>Marketing</h3>
          {this.state.menuItems.map((menuItem, index) => {
            return (
              <div key={index}>
                <Link prefetch href={menuItem[0]}>
                  <a className={pathname === menuItem[0] && "active"}>
                    <i>{menuItem[2]}</i>
                    <span>{menuItem[1]}</span>
                  </a>
                </Link>
              </div>
            );
          })}
        </div>
        <div className="content">{this.props.children}</div>
        <style jsx>{`
          main {
            display: flex;
            flex-direction: row;
            height: 90vh;
          }
          h3 {
            padding: 14px 16px;
          }
          .content {
            width: 80%;
            background: #f7f7f8;
          }
          .sidebar {
            width: 20%;
            list-style: none;
            text-align: left;
            border-right: 1px solid #808b97;
          }
          .sidebar a {
            text-decoration: none;
            padding: 14px 16px;
            color: #808b97;
            font-size: 16px;
          }
          .sidebar a i {
            padding: 5px;
            vertical-align: middle;
          }
        `}</style>
      </main>
    );
  }
}
