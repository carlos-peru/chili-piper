import React from "react";

export default class ExpandableBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hidden: true
    };
  }

  setMenuClass = () => {
    this.setState(prevState => {
      return {
        hidden: !prevState.hidden
      };
    });
  };

  render() {
    const { hidden } = this.state;
    return (
      <div className="box">
        {this.props.title}
        <a className="toggle" onClick={this.setMenuClass}>
          <i>{hidden ? "+" : "\u2212"}</i>
        </a>
        <div className={hidden ? "hidden" : ""}>{this.props.children}</div>
        <style jsx>{`
          .box {
            width: 90%;
          }
          .toggle {
            float: right;
          }
          .hidden {
            display: none;
          }
        `}</style>
      </div>
    );
  }
}
