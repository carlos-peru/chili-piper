import { Component } from "react";
import App from "../component/App";
import BookingLinks from "../component/BookingLinks";

export default class extends Component {
  render() {
    return (
      <App pathname="/booking-links">
        {" "}
        <BookingLinks />
      </App>
    );
  }
}
